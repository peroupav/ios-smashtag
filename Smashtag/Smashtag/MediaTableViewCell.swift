//
//  MediaTableViewCell.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 16/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit
import Twitter

class MediaTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaImageView: UIImageView!

    var media: MediaItem? {
        didSet {
            updateUI()
        }
    }
    
    var mediaImage: UIImage? {
        get {
            return mediaImageView.image
        }
    }
    
    private func updateUI() {
        fetchImage()
    }
    
    private func fetchImage() {
        if let url = media?.url {
            let session = URLSession(configuration: .default)
            // FIXME: weak self
            let datatask = session.dataTask(with: url) { (data: Data?, response, error) in
                // FIXME: check if still care
                if let imageData = data {
                    DispatchQueue.main.async {
                        self.mediaImageView.image = UIImage(data: imageData)
                    }
                }
            }
            datatask.resume()
        }
    }

}
