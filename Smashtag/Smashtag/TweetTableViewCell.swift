//
//  TweetTableViewCell.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 15/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit
import Twitter

class TweetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tweetProfileImageView: UIImageView!
    @IBOutlet weak var tweetCreatedLabel: UILabel!
    @IBOutlet weak var tweetUserLabel: UILabel!
    @IBOutlet weak var tweetTextLabel: UILabel!
    
    var tweet: Twitter.Tweet? {
        didSet {
            updateUI()
        }
    }
    
    // optional, protoze to dava smysl a protoze by jinak musela mit trida initializator, to nechces
    private var lastProfileImageURLSession: URLSession?
    // tahle funkce je hodne dlouha, asi fetchnuti image dat do samostatny funkce
    private func updateUI() {
        
        func setTweetText() -> Void {
            if let tweetContents = tweet {
                // FIXME: tim se asi rozbil pouzitej systemovej font (attributed string ma svuj default)
                let attributedTweetText = NSMutableAttributedString(string: tweetContents.text)
                var colors = TwitterColorPalette()
                
                func set(mention: Mention, toColor color: UIColor) -> Void {
                    // zajimavy, na staticky properties se pristopi pres struct, aniz by struct samotnej byla static
                    attributedTweetText.addAttributes([NSBackgroundColorAttributeName : color], range: mention.nsrange)
                }
                
                tweetContents.hashtags.forEach { mention in set(mention: mention, toColor: TwitterColorPalette.hashtags) }
                tweetContents.userMentions.forEach { mention in set(mention: mention, toColor: TwitterColorPalette.mentions) }
                
                tweetTextLabel?.attributedText = attributedTweetText
            }
        }
        
        func setProfileImage() -> Void {
            tweetProfileImageView?.image = nil
            if let profileImageURL = tweet?.user.profileImageURL {
                let session = URLSession(configuration: .default)
                lastProfileImageURLSession = session
                let datatask = session.dataTask(with: profileImageURL) { [weak self] (data: Data?, response, error) in
                    if let imageData = data {
                        // check if still relevant
                        if self?.lastProfileImageURLSession == session {
                            DispatchQueue.main.async {
                                self?.tweetProfileImageView?.image = UIImage(data: imageData)
                            }
                        }
                    }
                }
                datatask.resume()
            }
        }
        
        func setCreatedDate() -> Void {
            if let created = tweet?.created {
                let formatter = DateFormatter()
                if Date().timeIntervalSince(created) > 24*60*60 {
                    formatter.dateStyle = .short
                } else {
                    formatter.timeStyle = .short
                }
                tweetCreatedLabel?.text = formatter.string(from: created)
            } else {
                tweetCreatedLabel?.text = nil
            }
        }
        
        
        tweetUserLabel?.text = tweet?.user.description
        setTweetText()
        setCreatedDate()
        setProfileImage()
    }
    
    struct TwitterColorPalette {
        static let hashtags = UIColor.green
        static let mentions = UIColor.yellow
    }
}
