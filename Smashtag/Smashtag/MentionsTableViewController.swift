//
//  MentionsTableViewController.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 16/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit
import Twitter

class MentionsTableViewController: UITableViewController {
    
    private var tweetMentions: [TweetMention] = []
    
    var tweet: Twitter.Tweet? {
        didSet{
            // je to vyprazdneni k necemu? recykluje se to?
            tweetMentions = []
            // CHECKITOUT hustyy guard
            guard let tweet = tweet else {
                return
            }
            if !tweet.media.isEmpty         { tweetMentions.append(.media(tweet.media)) }
            if !tweet.hashtags.isEmpty      { tweetMentions.append(.hashtags(tweet.hashtags)) }
            if !tweet.urls.isEmpty          { tweetMentions.append(.urls(tweet.urls)) }
            if !tweet.userMentions.isEmpty  { tweetMentions.append(.userMentions(tweet.userMentions)) }
        }
    }
    
    private enum TweetMention {
        case hashtags([Mention])
        case media([MediaItem])
        case urls([Mention])
        case userMentions([Mention])
        
        var count: Int {
            switch self {
            case .hashtags(let items): return items.count
            case .media(let items): return items.count
            case .urls(let items): return items.count
            case .userMentions(let items): return items.count
            }
        }
        
        var mentions: [Mention] {
            switch self {
            case .hashtags(let items): return items
            case .urls(let items): return items
            case .userMentions(let items): return items
            default: return []
            }
        }
        
        var description: String {
            switch self {
            case .hashtags: return "Hashtags"
            case .media: return "Media"
            case .urls: return "URLs"
            case .userMentions: return "User mentions"
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tweetMentions.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetMentions[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tweetMentions[section].description
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tweetMention = tweetMentions[indexPath.section]
        switch tweetMention {
        case .media(let items):
            let cell = tableView.dequeueReusableCell(withIdentifier: "media", for: indexPath)
            (cell as! MediaTableViewCell).media = items[indexPath.row]
            return cell
        case .urls(let items):
            let cell = tableView.dequeueReusableCell(withIdentifier: "url", for: indexPath)
            cell.textLabel?.text = items[indexPath.row].keyword
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "mention", for: indexPath)
            cell.textLabel?.text = tweetMention.mentions[indexPath.row].keyword
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tweetMention = tweetMentions[indexPath.section]
        switch tweetMention {
        case .media(let items):
            let mediaItem = items[indexPath.row]
            return view.frame.size.width / CGFloat(mediaItem.aspectRatio)
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mentions = tweetMentions[indexPath.section]
        switch mentions {
        case .urls(let urls):
            let urlkeyword = urls[indexPath.row].keyword
            if let url = URL(string: urlkeyword) {
                UIApplication.shared.open(url)
            }
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "backToSearch":
                if let mentionCell = sender as? UITableViewCell,
                    let destination = segue.destination as? TweetTableViewController,
                    let indexPath = tableView.indexPath(for: mentionCell) {
                    switch tweetMentions[indexPath.section] {
                    case .hashtags(let items):
                        destination.searchTextFromSegue = items[indexPath.row].keyword
                    case .userMentions(let items):
                        destination.searchTextFromSegue = items[indexPath.row].keyword
                    default:
                        return
                    }
                }
            case "toImage":
                if let mediaCell = sender as? MediaTableViewCell,
                    let imageVC = segue.destination as? MediaViewController {
                    imageVC.image = mediaCell.mediaImage
                }
            default:
                break
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPopToRootButton()
    }
}
