//
//  TweetTableViewController.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 15/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit
import Twitter

class TweetTableViewController: UITableViewController, UITextFieldDelegate {

    // prvni vec co je potreba po vytvoreni controlleru: zamyslet se, what is my model?
    private var tweets = [Array<Twitter.Tweet>]()
    private var recentSearch = RecentSearchKeys()
    
    var searchTextFromSegue: String?
    // to bude public
    var searchText: String? {
        didSet {
            searchTextField?.text = searchText
            searchTextField?.resignFirstResponder()
            tweets.removeAll()
            // kdyz zmenim data, musim to hned rict view
            tableView.reloadData()
            searchForTweets()
            title = searchText
            
            recentSearch.add(key: searchText!)
        }
    }
    
    private func twitterRequest() -> Twitter.Request? {
        if let query = searchText, !query.isEmpty {
            return Twitter.Request(search: query, count: 100)
        }
        return nil
    }
    
    private var lastTwitterRequest: Twitter.Request?
    private func searchForTweets() {
        if let request = twitterRequest() {
            lastTwitterRequest = request
            request.fetchTweets { [weak self] newTweets in
                if request == self?.lastTwitterRequest {
                    DispatchQueue.main.async {
                        self?.tweets.insert(newTweets, at: 0)
                        self?.tableView.insertSections([0], with: .fade)
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField! {
        didSet{
            searchTextField.delegate = self
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // make sure it's me
        if textField == searchTextField {
            searchText = textField.text
        }
        // do what you normally do
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let fromSeque = searchTextFromSegue {
            searchText = fromSeque
        }
        
        // Deiman to ma tady, ale kdyz se pouzije prepare(toSeque.. tak jeste neni nastavena hodnota navigationController
        // Jemu to funguje, ale protoze nepouziva prepare (pouziva, ale nic v nem nenastavuje)
        // https://stackoverflow.com/questions/10982567/why-is-self-navigationcontroller-null-in-viewdidload
        // setPopToRootButton()
        // vyreseno, viz email konverzace s Deimanem, predmet: Smashtag project related question
        
        // 4 debugging
        if searchText == nil {
            searchText = "#olomouc"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // hlida zda tlacitko jiz neexistuje, aby ho nepridavalo podruhy
        setPopToRootButton()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tweets.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tweetCell = tableView.dequeueReusableCell(withIdentifier: "Tweet", for: indexPath) as! TweetTableViewCell
        tweetCell.tweet = tweets[indexPath.section][indexPath.row]

        return tweetCell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "tweetDetail":
                if let cell = sender as? TweetTableViewCell {
                    let indexPath = tableView.indexPath(for: cell)
                    if let mentionMVC = segue.destination.contents as? MentionsTableViewController,
                        let tweetIndexPath = indexPath {
                            mentionMVC.tweet = tweets[tweetIndexPath.section][tweetIndexPath.row]
                    }
                }
            default:
                break
            }
        }
    }
}

extension UIViewController {
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        }
        return self
    }
}
