//
//  MediaViewController.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 22/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit

class MediaViewController: UIViewController {

    // what is my model
    var image: UIImage? {
        didSet{
            imageView = UIImageView(image: image)
        }
    }
    
    fileprivate var imageView: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet{
            scrollView.delegate = self
            scrollView.addSubview(imageView)
            scrollView.minimumZoomScale = 0.1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.addSubview(imageView)
        scrollView.contentSize = imageView.frame.size
    }
    
    override func viewDidLayoutSubviews() {
        zoomToAspectFit()
    }
    
    private func zoomToAspectFit() {
        // to je asi fuk jestli bounds nebo frame
        // vyzkouset image rotovat a pak si hrat s bounds a frame, abych to pochopil
        
        // !! je potreba mit ve storyboardu nastavenej top 
        let imageSize = imageView.bounds.size
        let scrollViewSize = scrollView.bounds.size
        
        let zoomScaleForWidth = scrollViewSize.width / imageSize.width
        let zoomScaleForHeight = scrollViewSize.height / imageSize.height
        
        scrollView.setZoomScale(max(zoomScaleForWidth, zoomScaleForHeight), animated: true)
        
        let contentOffSetX = (scrollView.contentSize.width - scrollView.bounds.width) / 2
        let contentOffSetY = (scrollView.contentSize.height - scrollView.bounds.height) / 2
        scrollView.contentOffset = CGPoint(x: contentOffSetX, y: contentOffSetY)
    }
    
}

extension MediaViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
