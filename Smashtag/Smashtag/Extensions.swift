//
//  Extensions.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 16/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setPopToRootButton(from level: Int = popToRootViewControllerStackDepth)
    {
        if let controllers = navigationController?.viewControllers, controllers.count > level {
            // .contains rethrows, co to znamena?
            let toRootButtonIsThere = navigationItem.rightBarButtonItems?.contains { return $0.action == #selector(toRootViewController)} ?? false
            
            if !toRootButtonIsThere {
                // co je target?
                let toRootButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(toRootViewController))
                let rightBarButtons = [toRootButton] + (navigationItem.rightBarButtonItems ?? [])
                navigationItem.setRightBarButtonItems(rightBarButtons, animated: true)
            }
        }
    }
    
    func toRootViewController() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    static let popToRootViewControllerStackDepth = 2
}
