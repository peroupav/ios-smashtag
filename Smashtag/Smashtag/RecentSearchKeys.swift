//
//  RecentSearchKeys.swift
//  Smashtag
//
//  Created by Pavel Peroutka on 23/06/2017.
//  Copyright © 2017 Pavel Peroutka. All rights reserved.
//

import Foundation

class RecentSearchKeys {
    
    private let limit = 100
    
    private var searchKeys: [String] {
        get {
            return UserDefaults.standard.array(forKey: "search3") as? [String] ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "search3")
        }
    }
    
    func add(key: String) {
        // FIXME: toto nefunguje
//        let dropped = searchKeys.drop { $0.lowercased() == key.lowercased() }
//        searchKeys = Array(dropped)
        
        for (index, searchKey) in searchKeys.enumerated() {
            if searchKey.lowercased() == key.lowercased() {
                searchKeys.remove(at: index)
                break
            }
        }
        
        searchKeys.append(key)
        if searchKeys.count > limit {
            searchKeys.removeFirst()
        }
        UserDefaults.standard.synchronize()
    }
    
    var last: String? {
        return searchKeys.first
    }
    
    var count: Int {
        return searchKeys.count
    }
    
    func key(at index: Int) -> String {
        return searchKeys[count - 1 - index]
    }
    
}
